﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class AddForceOnCollision : MonoBehaviour
{

    [SerializeField] private Vector2 _throwForce;
    [SerializeField] private bool _useNormal;

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.GetComponent<Physics>() != null)
        {
            Vector2 tForce = _throwForce;
            if (_useNormal)
            {
                if (coll.transform.position.x < transform.position.x)
                {
                    tForce.x = -tForce.x;
                }
            }
            coll.gameObject.GetComponent<Physics>().Halt();
            coll.gameObject.GetComponent<Physics>().AddForce(tForce);
        }
    }

}
