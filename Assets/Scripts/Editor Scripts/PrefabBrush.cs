﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu]
[CustomGridBrush(false, true, false, "New Prefab Brush")]
public class PrefabBrush : GridBrushBase
{
    [SerializeField] private GameObject _prefab;

    public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position)
    {
        if (brushTarget.layer == 31) return;

        if (GetObjectInCell(grid, brushTarget.transform, position) != null) return;
        
        GameObject tObject = Instantiate(_prefab, grid.CellToWorld(position), Quaternion.identity);
        tObject.transform.SetParent(brushTarget.transform);
        Undo.RegisterCreatedObjectUndo(tObject, "Created " + tObject.name);
    }

    public override void Erase(GridLayout grid, GameObject brushTarget, Vector3Int position)
    {
        if (brushTarget.layer == 31) return;
        Transform tErased = GetObjectInCell(grid, brushTarget.transform, new Vector3Int(position.x, position.y, 0));
        if (tErased != null)
        {
            Undo.DestroyObjectImmediate(tErased.gameObject);
        }
    }

    private static Transform GetObjectInCell(GridLayout grid, Transform parent, Vector3Int position)
    {
        int childCount = parent.childCount;
        Vector3 min = grid.LocalToWorld(grid.CellToLocalInterpolated(position));
        Vector3 max = grid.LocalToWorld(grid.CellToLocalInterpolated(position + Vector3Int.one));
        Bounds bounds = new Bounds((max + min)*0.5f, max-min);

        for (int i = 0; i < childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (bounds.Contains(child.position)) return child;
        }

        return null;
    }
}
