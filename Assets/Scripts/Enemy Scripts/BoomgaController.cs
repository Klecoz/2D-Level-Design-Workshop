﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomgaController : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 2;
    [SerializeField] private ContactFilter2D _wallBumpFilter;

    private HealthManager _healthManager;
    private Animator _animator;
    private Physics _physics;
    private Collider2D _collider;
    private Vector2 _direction;

    private RaycastHit2D[] _raycastHit2D = new RaycastHit2D[100];
    private bool _active = false;
    private bool _dead = false;

    void Start()
    {
        _healthManager = GetComponent<HealthManager>();
        _animator = GetComponent<Animator>();
        _physics = GetComponent<Physics>();
        _collider = GetComponent<Collider2D>();
        _healthManager.OnDeath(() =>
        {
            _dead = true;
            _active = false;
            _physics.Halt();
            _collider.enabled = false;
            _animator.SetBool("Dead", true);
            StartCoroutine(DeathCoroutine());
        });

        _direction = Vector2.left;
    }

    void Update()
    {
        if (Vector2.Distance(GameManager.instance._player.transform.position, transform.position) < 20)
        {
            _active = true;
        }
        else
        {
            _active = false;
        }

        if (!_active || _dead) return;
        _physics.AddForce(_moveSpeed * _direction);

        if (_collider.Raycast(_direction, _wallBumpFilter, _raycastHit2D, 0.6f) != 0)
        {
            Debug.Log("Wall bump", gameObject);
            _direction.x *= -1;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        ContactPoint2D[] tContactPoint2Ds = new ContactPoint2D[100];
        for (int i = 0; i < coll.GetContacts(tContactPoint2Ds); i++)
        {
            if (tContactPoint2Ds[i].collider.CompareTag("Player"))
            {
                if (Mathf.Acos(Vector2.Dot(tContactPoint2Ds[i].point - new Vector2(transform.position.x, transform.position.y), Vector2.up)) * Mathf.Rad2Deg < 60)
                {
                    _healthManager.Damage(5);
                    tContactPoint2Ds[i].collider.GetComponent<Physics>().AddForce(0, 20);
                }
                else
                {
                    tContactPoint2Ds[i].collider.GetComponent<HealthManager>().Damage(1);
                }
                break;
            }
        }
    }

    IEnumerator DeathCoroutine()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }

}
