﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject _player;
    private HealthManager _playerHealth;

    public static GameManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (instance != this)
        {
            Destroy(instance.gameObject);
        }
    }
    
    void Start()
    {
        _playerHealth = _player.GetComponent<HealthManager>();
        _playerHealth.OnDeath(GameOver);
    }

    public void GameOver()
    {
        Debug.Log("Game Over");
        UIManager.instance.GameOver();
    }

    public void LevelComplete()
    {
        Debug.Log("Level Complete");
        UIManager.instance.LevelComplete();
        _player.GetComponent<PlayerController>().CanControl(false);
        _player.GetComponent<Physics>().Halt();
    }
}
