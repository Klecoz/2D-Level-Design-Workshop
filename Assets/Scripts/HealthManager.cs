﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class HealthManager : MonoBehaviour
{

    [SerializeField] private int _maxHealth;

    private int _currentHealth;

    public int currentHealth
    {
        get { return _currentHealth; }
        private set { }
    }

    public bool _invincible;

    public delegate void DamageDelegate();

    private DamageDelegate _damageDelegate;
    private DamageDelegate _deathDelegate;

    void Start()
    {
        _currentHealth = _maxHealth;
    }

    public void Damage(int aDamage)
    {
        if (_invincible)
            return;

        Debug.Log(gameObject.name + " damaged: " + aDamage + " points");
        _currentHealth -= aDamage;
        if (_damageDelegate != null) _damageDelegate();
        if (_currentHealth <= 0 && _deathDelegate != null) _deathDelegate();
    }

    public void Heal(int aHeal)
    {
        _currentHealth += aHeal;
    }

    public void OnDeath(DamageDelegate aDamageDelegate)
    {
        _deathDelegate += aDamageDelegate;
    }

    public void OnDamage(DamageDelegate aDamageDelegate)
    {
        _damageDelegate += aDamageDelegate;
    }


}
