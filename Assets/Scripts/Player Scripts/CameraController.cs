﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    [SerializeField] private float _cameraRange = 2;
    
    private Vector2 _offset;

    private Physics _playerPhysics;

    void Start()
    {
        _playerPhysics = gameObject.GetComponentInParent<Physics>();
    }

    void Update()
    {
        _offset = Vector2.Lerp(transform.localPosition, _playerPhysics.velocity.normalized*_cameraRange, Time.deltaTime);

        transform.localPosition = new Vector3(
            _offset.x,
            _offset.y,
            transform.localPosition.z);
    }

}
