﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{

    [SerializeField] private float _minWalkVelocity = 0.3f;

    private Physics _physics;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    private PlayerController _playerController;
    private HealthManager _playerHealth;
    void Start()
    {
        _physics = gameObject.GetComponent<Physics>();
        _animator = gameObject.GetComponent<Animator>();
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _playerController = gameObject.GetComponent<PlayerController>();
        _playerHealth = gameObject.GetComponent<HealthManager>();
    }

    void Update()
    {
        _animator.SetFloat("Vertical Speed", _physics.velocity.y);

        if (_physics.isGrounded)
        {
            _animator.SetBool("Grounded", true);
            if (Mathf.Abs(_physics.velocity.x) > _minWalkVelocity)
            {
                _animator.SetBool("Walking", true);
            }
            else
            {
                _animator.SetBool("Walking", false);
            }
        }
        else
        {
            _animator.SetBool("Grounded", false);
            _animator.SetBool("Walking", false);
        }

        if (_playerController.damaged)
        {
            _animator.SetBool("Damaged", true);
        }
        else
        {
            _animator.SetBool("Damaged", false);
            if (_physics.velocity.x > 0) _spriteRenderer.flipX = false;
            if (_physics.velocity.x < 0) _spriteRenderer.flipX = true;
        }

        if (_playerHealth._invincible)
        {
            _spriteRenderer.enabled = (Time.time%0.25f) < 0.125f;
        }
        else
        {
            _spriteRenderer.enabled = true;
        }
        
    }
}
