﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text _healthText;
    [SerializeField] private GameObject _gameOverPanel, _youWinPanel;
    [SerializeField] private HealthManager _playerHealthManager;

    public static UIManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(instance.gameObject);
        }
    }


    void Start()
    {
        _youWinPanel.SetActive(false);
        _gameOverPanel.SetActive(false);
    }

    void Update()
    {
        if (_playerHealthManager.currentHealth > 0) _healthText.text = "HEALTH: " + _playerHealthManager.currentHealth;
        else _healthText.text = "HEALTH: 0";
    }

    public void LevelComplete()
    {
        _youWinPanel.SetActive(true);
    }

    public void GameOver()
    {
        _gameOverPanel.SetActive(true);
    }
}
